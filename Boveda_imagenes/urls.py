from django.contrib import admin
from django.urls import path

from login.views import register

#importacion para usar las plantillas de login y logout
from django.contrib.auth.views import LoginView, LogoutView

#para usrar la importacion de las imagenes
from django.conf.urls.static import static
from django.conf import settings

#importacion de vistas
from index.views import inicio
from uoload_image.views import loadImage, cargar, borrar_Imagen


urlpatterns = [
    path('admin/', admin.site.urls),
    path('registro/', register, name='registro'),
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='inicio.html'), name='logout'),
    path('delete/<int:id_img>/', borrar_Imagen, name='delete'),
]


urlpatterns += [
    path('', inicio, name='inicio'),
    path('feed/', loadImage, name='feed'),
]


urlpatterns += [
    path('cargar/', cargar, name='cargar'),
]

urlpatterns += static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)
