from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages

def register(request):

	if request.method == 'POST':
		form = UserCreationForm(request.POST)

		if form.is_valid():
			form.save()
			username = form.cleaned_data['username']
			messages.success(request, f'El usuario {username} se ha creado exitosamente')

	else:
		form = UserCreationForm()


	context = {'form':form}
	return render(request, 'registro.html', context)