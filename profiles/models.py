from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class profile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)



def create_profile(sender, instance, created, **kwargs):
	if created:
		profile.objects.create(user=instance)


post_save.connect(create_profile, sender=User)