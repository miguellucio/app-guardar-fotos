from django.apps import AppConfig


class UoloadImageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'uoload_image'
