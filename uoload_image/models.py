from django.db import models

class upload(models.Model):

    id_img = models.AutoField(primary_key=True, unique=True)
    img = models.ImageField(null=True)
    desc = models.CharField(max_length=50)
    date = models.DateField()
