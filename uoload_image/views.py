from django.shortcuts import render, redirect
from .models import upload
from .form import uploadForm

from profiles.models import profile
from django.contrib.auth.models import User

def loadImage(request):

    if request.method == 'GET':

        if request.user.is_authenticated:
            Upload = upload.objects.all()
            context = {'upload': Upload}
            return render(request, 'feed.html', context)

        else:
            return redirect('login')

    




def cargar(request):

    if request.method == 'GET':

        if request.user.is_authenticated:
            form = uploadForm()
            context = {
            'form': form
            }

        else:
            return redirect('login')

    else:
        form = uploadForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            return redirect('feed')


    return render(request, 'carga.html', context)




def borrar_Imagen(request, id_img):

    if request.user.is_authenticated:
        Upload = upload.objects.get(id_img = id_img)
        Upload.delete()
        return redirect('feed')